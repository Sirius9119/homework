const burgerMenu = document.querySelector('.burger-menu__btn');
burgerMenu.classList.remove('burger-menu__active');
burgerMenu.addEventListener("click", () => {
    burgerMenu.classList.toggle('burger-menu__active');
});