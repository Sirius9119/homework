import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types'
import './MainPost.scss'
import HeartIcon from "../icon/HeartIcon/HeartIcon.jsx";
import SendIcon from "../icon/SendIcon/SendIcon.jsx";
import CommentIcon from "../icon/CommentIcon/CommentIcon.jsx";
import SaveIcon from "../icon/SaveIcon/SaveIcon.jsx";
import MoreIcon from "../icon/MoreIcon/MoreIcon.jsx";
import Comment from "../Comment/Comment.jsx";
import AddComment from "../AddComment/AddComment.jsx";
import {Link, useOutletContext} from "react-router-dom";

function MainPost({postAuthor, postAuthorAvatar,postId, key, postImg, comment_author, comment_text, commentLength, likesLength, handleModal, handleCurrentPost}) {
    const {
        toggleLike,
        loginUser,
        likesData,


    } = useOutletContext()
    const [isLiked, setIsLiked] = useState(false)
    const handleLike = () => {

        setIsLiked(!isLiked)
    }


    useEffect(()=> {
        if(likesData.some(like => like.post_id === postId && like.user_id === loginUser.ID)) {
            setIsLiked(true);
        }
    },[likesData, loginUser.ID, postId]);


    return (

        <div className='main-post' id={key}>
            <div className='main-post__author'>
                <Link to={postAuthor} className='main-post__author-avatar'>
                    <img src={postAuthorAvatar} alt='avatar'/>
                </Link>
                <Link to={postAuthor} className='main-post__author-name'>{postAuthor}</Link>
                <MoreIcon/>
            </div>
            <div className='main-post__img'onDoubleClick={()=>{
                handleLike()
                toggleLike(postId, isLiked)
            } } >
                <img src={postImg} alt='img'/>
            </div>
            <div className='main-post__icon-wrapper'>
                <HeartIcon
                    isLiked={isLiked}
                    handleLike={()=>{
                        handleLike ()
                        toggleLike(postId, isLiked)
                    }}
                />
                <CommentIcon/>
                <SendIcon/>
                <SaveIcon/>
            </div>
            <p className='main-post__views'>{likesLength} нравится </p>
            <div className='main-post__comments-wrapper'>
                <Comment
                    comment_author={comment_author}
                    comment_text={comment_text}
                />
                {commentLength > 1
                    &&
                    <p className='main-post__comments-more'
                       onClick={()=>{
                           handleModal()
                           handleCurrentPost({postId})

                       }}  >Посмотреть все комментарии ({commentLength})</p>}
            </div>
            <AddComment/>


        </div>
    );
}

MainPost.propTypes = {
    postAuthor: PropTypes.string,
    postId: PropTypes.string,
    postImg: PropTypes.string,
    postAuthorAvatar: PropTypes.string,
    comment_author: PropTypes.string,
    comment_text: PropTypes.string,
    commentLength: PropTypes.number,
    likesLength: PropTypes.number,
    handleModal: PropTypes.func,
    handleCurrentPost: PropTypes.func,
    key: PropTypes.string,
}
export default MainPost;