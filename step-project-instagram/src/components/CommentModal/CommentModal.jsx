import React from 'react';
import './CommentModal.scss'
import Comment from "../Comment/Comment.jsx";
import PropTypes from 'prop-types'

function CommentModal({comment_author, comment_text, commentAuthorAvatar, commentAuthorId, key}) {


    return (
        <div className='modal__comment' id={key}>
            <div className='modal__comment-avatar'>
                <img src={commentAuthorAvatar} alt='avatar'/>
            </div>
            <Comment
                comment_author={comment_author}
                comment_text={comment_text}
            />
        </div>
    );
}
CommentModal.propTypes = {
    comment_author: PropTypes.string,
    comment_text: PropTypes.string,
    commentAuthorAvatar: PropTypes.string,
    commentAuthorId: PropTypes.string,
    key: PropTypes.string
}
export default CommentModal;