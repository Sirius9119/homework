import React from 'react';
import './Comment.scss'
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";

function Comment({comment_author, comment_text}) {
    return (
        <div className="comment-wrap">
            <Link className='comment-author' to={comment_author}>{comment_author}</Link>
            <span className="comment-text">{comment_text}</span>
        </div>
    );
}

Comment.propTypes = {
    comment_author: PropTypes.string,
    comment_text: PropTypes.string
}
export default Comment;