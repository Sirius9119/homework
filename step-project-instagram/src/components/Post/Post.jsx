import React from 'react';
import './Post.scss'
import PropTypes from 'prop-types'

function Post({postId, postImg, key, commentLength, likesLength, handleCurrentPost, handleModal}) {

    return (
        <div className="post-card" id={key} onClick={ ()=>{
            handleModal()
            handleCurrentPost({postId})
        }}>
            <img src={postImg} alt='image'/>

                <div className="post-overlay">
                    <div className="post-icons">
                        <i className="fa fa-heart"></i>
                        <span className="likes">{likesLength}</span>
                        <i className="fa fa-comment"></i>
                        <span className="comments">{commentLength}</span>
                    </div>
                </div>
        </div>


);
}
Post.propTypes = {
    postImg: PropTypes.string,
    key: PropTypes.string,
    commentLength: PropTypes.number,
    likesLength: PropTypes.number,
    handCurrentPost: PropTypes.func,
    handleCurrentPost: PropTypes.func,
    postId: PropTypes.string,
    handleModal: PropTypes.func
}
export default Post;