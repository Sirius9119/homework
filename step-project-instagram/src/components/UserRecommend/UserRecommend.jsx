import React, {useEffect, useState} from 'react';
import './UserRecommend.scss'
import PropTypes from 'prop-types'
import {Link, useOutletContext} from "react-router-dom";
function UserRecommend({userName,avatar, key, userId}) {
    const {
        followData,
        loginUser,
        toggleFollow

    }= useOutletContext()

    const [isFollow, setIsFollow] = useState(false)
    const handleFollow = () => {
        setIsFollow(!isFollow)
    }

    useEffect(() => {
        if(followData.some(follow => follow.userToFollow_id === userId && follow.user_id === loginUser.ID)) {
            setIsFollow(true);
        }
    }, [])
    return (
        <div id={key}  className='follow__wrap' >
            <Link className="following" to={userName}>
                <div className='following__avatar'>
                    <img src={avatar} alt='avatar' />
                </div>
            </Link>
            <Link className='following__name' to={userName}>{userName}</Link>
            {!isFollow && (
                <button
                    className='btnToFollow'
                    onClick={() => {
                        handleFollow();
                        toggleFollow(userId, isFollow);
                    }}
                >
                    Подписаться
                </button>
            )}
        </div>

    );
}

UserRecommend.propTypes = {
    userName: PropTypes.string,
    avatar: PropTypes.string,
    key: PropTypes.string,
    userId: PropTypes.string

}
export default UserRecommend;