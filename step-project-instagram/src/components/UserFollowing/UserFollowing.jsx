import React from 'react';
import './UserFollowing.scss'
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
function UserFollowing({userName,avatar, key}) {
    return (
        <div id={key} className='follow__wrap'>
            <Link className="following" to={userName}>
                <div className='following__avatar'>
                    <img src={avatar} alt='avatar' />
                </div>
            </Link>
            <Link className='following__name' to={userName}>{userName}</Link>
        </div>
    );
}

UserFollowing.propTypes = {
    userName: PropTypes.string,
    avatar: PropTypes.string,
    key: PropTypes.string
}
export default UserFollowing;