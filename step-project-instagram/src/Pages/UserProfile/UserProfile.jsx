import React, {useEffect, useState} from 'react';
import './UserProfile.scss'
import MoreIcon from "../../components/icon/MoreIcon/MoreIcon.jsx";
import Post from "./../../components/Post/Post.jsx";
import {useLocation, useOutletContext} from "react-router-dom";
import Modal from "../../components/Modal/index.js";

function UserProfile() {
    const {
        isModalPost,
        handleOutside,
        usersData,
        likesData,
        commentsData,
        postData,
        followData,
        handleModal,
        handleCurrentPost,
        toggleFollow,
        loginUser

    } = useOutletContext()

    const [currentUser, setCurrentUser] = useState({})
    const location = useLocation()
    const pathParts = location.pathname.split('/');
    const currentUserName = pathParts[pathParts.length - 1];
    const [isFollow, setIsFollow] = useState(false)
    const handleFollow = () => {
        setIsFollow(!isFollow)
    }

    useEffect(() => {
        if (followData.some(follow => follow.userToFollow_id === currentUser.ID && follow.user_id === loginUser.ID)) {
            setIsFollow(true);
        }
    }, [followData, currentUser.ID, loginUser.ID])


    useEffect(() => {
        function setCurrentUserFunction(userName) {
            const user = usersData.find(user => user.username === userName);
            if (user) {
                setCurrentUser(user);
            }
        }

        setCurrentUserFunction(currentUserName);
    }, [usersData]);


    const allUserPosts = postData.filter(post => post.user_id === currentUser.ID)
    const userFollows = followData.filter(user => user.user_id === currentUser.ID)
    const userFollowins = followData.filter(user => user.userToFollow_id === currentUser.ID)
    const userFollowingsNames = userFollowins.map(follow => {
        const user = usersData.find(userData => userData.ID === follow.user_id);

        return user
            ? {username: user.username, avatar: user.avatar}
            : {username: null, avatar: null};
    });

    userFollowins.sort((a, b) => a.date - b.date)
    allUserPosts.sort((a, b) => b.post_date - a.post_date)


    return (

        <>
            <div>
                <div className="user-profile">
                    <div className="user-profile__avatar">
                        <img
                            src={currentUser.avatar}
                            alt="avatar"/>
                    </div>
                    <div className="user-profile__info">
                        <div className="user-profile__wrapNick">
                            <h2 className="user-profile__nickname">{currentUser.username}</h2>
                            {currentUser.ID !== loginUser.ID &&
                                <>
                                    <button className="user-profile__btn-follower"
                                            onClick={() => {
                                                handleFollow()
                                                toggleFollow(currentUser.ID, isFollow)
                                            }
                                            }>{isFollow ? "Отписаться" : "Подписаться"}</button>
                                    <button className="user-profile__btn">▾</button>
                                </>
                            }

                            <MoreIcon/>

                        </div>
                        <ul className="user-profile__list-count">
                            <li className="user-profile__count-posts"><span>{allUserPosts.length}</span> публикаций</li>
                            <li className="user-profile__count-follows"><span>{userFollows.length}</span> подписчиков
                            </li>
                            <li className="user-profile__count-followings"><span>{userFollowins.length}</span> подписок
                            </li>
                        </ul>
                        <h3 className="user-profile__realName">{currentUser.real_name}</h3>
                        {userFollowingsNames.length !== 0 &&
                            <div className="user-profile__userFollows">
                                <p>Подписаны</p>
                                <p>{userFollowingsNames[userFollowingsNames.length - 1].username}
                                    {userFollowingsNames.length > 1
                                        &&
                                        <span> и еще {userFollowingsNames.length - 1}</span>}

                                </p>

                            </div>

                        }
                    </div>

                </div>
                <div className="user-post__header">
                    <a className="user-post__allPublic" href="#">
                        <svg fill="currentColor" height="12"
                             role="img" viewBox="0 0 24 24" width="12"><title></title>
                            <rect fill="none" height="18" stroke="currentColor" stroke-linecap="round"
                                  stroke-linejoin="round" stroke-width="2" width="18" x="3" y="3"></rect>
                            <line fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2" x1="9.015" x2="9.015" y1="3" y2="21"></line>
                            <line fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2" x1="14.985" x2="14.985" y1="3" y2="21"></line>
                            <line fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2" x1="21" x2="3" y1="9.015" y2="9.015"></line>
                            <line fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2" x1="21" x2="3" y1="14.985" y2="14.985"></line>
                        </svg>
                        <span>ПУБЛИКАЦИИ</span>
                    </a>

                </div>
                <div className="user-post__wrapper">
                    {allUserPosts.map(post => {
                        const allComments = commentsData.filter(comment => comment.post_id === post.post_id)
                        const allLikes = likesData.filter(like => like.post_id === post.post_id)

                        return (
                            <Post
                                postId={post.post_id}
                                key={post._id}
                                postImg={post.post_image}
                                commentLength={allComments.length}
                                likesLength={allLikes.length}
                                handleModal={handleModal}
                                handleCurrentPost={handleCurrentPost}
                            />
                        )
                    })}


                </div>
            </div>
            {isModalPost && <Modal
                handleOutside={handleOutside}
            />}


        </>
    );
}

export default UserProfile;