import React, {useEffect, useState} from 'react';
import './MainProfile.scss'
import MainPost from "../../components/MainPost/index.js";
import UserFollowing from "../../components/UserFollowing/index.js";
import UserRecommend from "../../components/UserRecommend/index.js";
import Modal from "../../components/Modal/index.js";
import {Link, useOutletContext} from "react-router-dom";
import InfiniteScroll from 'react-infinite-scroll-component';

function MainProfile() {
    const {
        isModalPost,
        handleModal,
        handleOutside,
        usersData,
        loginUser,
        followData,
        likesData,
        commentsData,
        postData,
        handleCurrentPost

    } = useOutletContext()
    const usersFollowArr = followData.filter(user => user.user_id === loginUser.ID)
    const usersRecommendArr = usersData.filter(user => user.ID !== loginUser.ID && !usersFollowArr.some(follow => follow.userToFollow_id === user.ID));

    const newsLinePosts = postData.filter(post => usersFollowArr.some(user => user.userToFollow_id === post.user_id));
    postData.sort((a, b) => b.post_date - a.post_date);
    commentsData.sort((a, b) => b.date - a.date);

    // const [list, setList] = useState([]);
    // const [page, setPage] = useState(1);
    // const [hasMore, setHasMore] = useState(true);
    //
    // useEffect(() => {
    //     fetchMoreData(); // Загрузить первую партию данных
    // }, []); // Пустой массив зависимостей означает, что этот эффект будет запущен только один раз после монтирования компонента
    // console.log(list)
    // const fetchMoreData = () => {
    //     const nextPagePosts = newsLinePosts.slice((page - 1) * 3, page * 3);
    //
    //     if (nextPagePosts.length > 0) {
    //         setList((prevList) => [...prevList, ...nextPagePosts]);
    //         setPage((prevPage) => prevPage + 1);
    //     } else {
    //         setHasMore(false);
    //     }
    // };



    return (
        <>
            {/*<InfiniteScroll*/}
            {/*    dataLength={list.length}*/}
            {/*    next={fetchMoreData}*/}
            {/*    hasMore={hasMore}*/}
            {/*    loader={<h4>Loading...</h4>}*/}
            {/*    endMessage={<p>No more posts</p>}*/}
            {/*/>*/}
            <div className='news-line'>

                {newsLinePosts?.map(post => {
                    const user = usersData.find(user => user.ID === post.user_id);
                    const comment = commentsData.find(comment => comment.post_id === post.post_id);
                    const commentText = comment ? comment.comment_text : 'Нет коментариев';
                    const commentUser = comment ? usersData.find(user => user.ID === comment.user_id) : null;
                    const commentAuthor = commentUser ? commentUser.username : ''
                    const allComments = commentsData.filter(comment => comment.post_id === post.post_id)
                    const allLikes = likesData.filter(like => like.post_id === post.post_id)

                    return (
                        <MainPost
                            key={post._id}
                            postId={post.post_id}
                            postImg={post.post_image}
                            postAuthor={user.username}
                            postAuthorAvatar={user.avatar}
                            comment_author={commentAuthor}
                            comment_text={commentText}
                            commentLength={allComments.length}
                            likesLength={allLikes.length}
                            handleModal={handleModal}
                            handleCurrentPost={handleCurrentPost}
                        />
                    );
                })}

            </div>
            <div className='users-container'>
                <div className='main-user'>
                    <Link to={loginUser.username} className='main-user__avatar'>
                        <img src={loginUser.avatar} alt='avatar'/>
                    </Link>
                    <div className='main-user__info'>
                        <Link to={loginUser.username} className='main-user__nickName'>{loginUser.username}</Link>
                        <p className='main-user__name'>{loginUser.real_name}</p>
                    </div>

                </div>
                <div className='user-following'>
                    <div className='user-following__header'>
                        <p className='user-following__stories'>Ваши подсписки</p>
                        <p className='user-following__showAll'>Показать всех</p>
                    </div>
                    <div className='user-following__wrapper'>
                        {usersFollowArr.map((user) =>{
                            const userFollow = usersData.find(item => item.ID === user.userToFollow_id)

                            return (
                                <UserFollowing
                                    avatar={userFollow.avatar}
                                    userName={userFollow.username}
                                    key={user._id}
                                />

                                )
                            }
                        )
                        }


                    </div>
                </div>
                <div className='user-following'>
                    <div className='user-following__header'>
                        <p className='user-following__stories'>Рекомендации для Вас</p>
                        <p className='user-following__showAll'>Показать всех</p>
                    </div>
                    <div className='user-following__wrapper'>

                        {usersRecommendArr.map((user) => (
                            <UserRecommend
                                avatar={user.avatar}
                                userName={user.username}
                                key={user._id}
                                userId={user.ID}
                            />)
                        )}


                    </div>
                </div>
                <div className='about__wrapper'>
                    <a className='about__item' href='#'>Информация</a>
                    <a className='about__item' href='#'>Помощь</a>
                    <a className='about__item' href='#'>Пресса</a>
                    <a className='about__item' href='#'>API</a>
                    <a className='about__item' href='#'>Вакансии</a>
                    <a className='about__item' href='#'>Конфиденциальность</a>
                    <a className='about__item' href='#'>Условия</a>
                    <a className='about__item' href='#'>Места</a>
                    <a className='about__item' href='#'>Язык</a>
                    <a className='about__item' href='#'>Meta Verified</a>
                </div>
                <p className='inst_from_meta'>© 2023 INSTAGRAM FROM META</p>

            </div>
            {isModalPost && <Modal
                handleOutside={handleOutside}
            />}

        </>
    );
}

export default MainProfile;