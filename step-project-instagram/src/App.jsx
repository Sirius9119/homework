import './App.css'

import {Routes, Route} from "react-router-dom";
import PageWrapper from "./Pages/PageWrapper/index.js";
import MainProfile from "./Pages/MainProfile/index.js";
import UserProfile from "./Pages/UserProfile/index.js";

function App() {

    return(
        <>
            <Routes>
                <Route path = {'/'} element={<PageWrapper/>}>
                    <Route index element={<MainProfile/>}/>
                    <Route path = {'/:username'} element={<UserProfile/>}/>
                </Route>
            </Routes>
        </>
    )
}

export default App
