import {Router} from 'express';
import {v4 as uuidv4} from 'uuid';
import followMongo from "../moduls/followMongo.js";

const router = Router();

// router.post('/follows', async (req, res) => {
//     const {user_id} = req.body
//
//     try {
//         const follows = await followMongo.find({user_id: user_id});
//
//         res.status(200).json({status: true, follows});
//     } catch (err) {
//         res.status(200).json({status: false, error: err.message})
//     }
// })
router.get('/follows', async (req, res) => {
    try {
        const follows = await followMongo.find();

        res.status(200).json({status: true, follows});
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})

router.post('/follow', async (req, res) => {
    const {user_id, userToFollow_id} = req.body


    const newFollow = {
        follow_id: uuidv4(),
        user_id: user_id,
        userToFollow_id: userToFollow_id,

    }
    const follow = new followMongo(newFollow)
    try {
        await follow.save()

        res.status(200).json({status: true})
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})

router.delete('/follow/del', async (req, res) => {
    const {user_id, userToFollow_id} = req.body

    try {
        await followMongo.deleteOne({user_id: user_id, userToFollow_id: userToFollow_id})

        res.status(200).json({status: true})
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})

export default router