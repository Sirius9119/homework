import {Router} from 'express';
import postMongo from "../moduls/postMongo.js";


const router = Router();

router.get('/posts', async (req, res) => {
    try {
        const posts = await postMongo.find();

        res.status(200).json({status: true, posts});
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})


export default  router