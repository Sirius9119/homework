import {Router} from 'express';
import commentMongo from "../moduls/commentMongo.js";


const router = Router();

router.get('/comments', async (req, res) => {
    try {
        const comments = await commentMongo.find();

        res.status(200).json({status: true, comments});
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})


export default  router