import {Router} from 'express';
import userMongo from "../moduls/userMongo.js";


const router = Router();

router.get('/users', async (req, res) => {
    try {
        const users = await userMongo.find();
        res.status(200).json({status: true, users});

        // res.status(200).json(users);
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})


export default  router