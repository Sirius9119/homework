import {Schema, model} from 'mongoose';

const followMongo = new Schema({
    follow_id: String,
    user_id: String,
    userToFollow_id: String,
    date: { type: Number, default: Date.now },

})
    export default model('Follow', followMongo);