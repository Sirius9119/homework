import {Schema, model} from 'mongoose';


const commentMongo = new Schema({
    comment_id: String,
    post_id: String,
    user_id: String,
    comment_text: String,
    post_date: { type: Number, default: Date.now },
})
    export default model('Comment', commentMongo);