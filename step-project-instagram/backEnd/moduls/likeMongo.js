import {Schema, model} from 'mongoose';


const likeMongo = new Schema({
    like_id: String,
    post_id: String,
    user_id: String,
    date: { type: Number, default: Date.now },
})
    export default model('Like', likeMongo);