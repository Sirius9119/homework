// 1. Переменные объявленные через const нельзя перезаписывать в дальнейшем, они являются константой.
//     Переменные объявленные через let, можно перезаписывать и присваивать ей новое значение.
//     Объявление переменной через устаревшее значение var, так же можно перезаписывать значение в дальнейшем но они игнорируют
// блочную область видимости, в отличие от let и const.
// 2. Объявление переменной через устаревшее значение var считается плохой практикой потому что переменные var игнорируют
// блочную область видимости

"use strict"
let userName;
while (!userName) {
    userName = prompt('Введите свое имя!');
}
let userAge;
while (!isFinite(userAge)) {
    userAge = +prompt('Введите свой возраст!');
}
if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome,  ${userName}`);
    } else {
        alert('You are not allowed to visit this website')
    }
} else {
    alert(`Welcome,  ${userName}`);
}