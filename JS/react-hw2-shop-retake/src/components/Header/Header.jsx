import './Header.scss'
import {ReactComponent as Logo} from "./icons/logo.svg";
import {ReactComponent as Favorite} from "./icons/favorite.svg";
import {ReactComponent as Basket} from "./icons/basket.svg";
import PropTypes from 'prop-types'


function Header({basketLength,favoriteLength}) {
    return (
        <header className="header">
            <div className="container">
                <div className="header__wrapper">
                    <div className="header__logo">
                        <a href="#" className="logo">
                            <Logo/>
                        </a>
                    </div>
                    <nav className="header__actions">
                        <a href="#" className="header__actions-item">Shop</a>
                        <a href="#" className="header__actions-item">Men</a>
                        <a href="#" className="header__actions-item">Women</a>
                        <a href="#" className="header__actions-item">Combos</a>
                        <a href="#" className="header__actions-item">Joggers</a>
                    </nav>
                    <div className="header__favorites-list">

                        <span className="icon-favorite">
                        <span className="count-favorite">{favoriteLength}</span>
                            <Favorite/>
                    </span>
                        <span className="icon-basket">
                        <span className="count-basket">{basketLength}</span>
                        <Basket/>
                    </span>
                    </div>
                </div>
            </div>
        </header>
    )
}

Header.propTypes = {
    basketLength: PropTypes.number,
    favoriteLength:PropTypes.number
}

export default Header