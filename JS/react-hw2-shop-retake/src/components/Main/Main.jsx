import './Main.scss'
import ProductSection from "./components/ProductSection";
import PropTypes from 'prop-types'


function Main({product, handleModal, handleCurrentProduct,addFavorite}) {
    return (
        <main className="main container">
            <ProductSection
                product={product}
                handleModal={handleModal}
                handleCurrentProduct={handleCurrentProduct}
                addFavorite={addFavorite}
            />

        </main>
    )
}

Main.propTypes = {
    product: PropTypes.array,
    handleModal: PropTypes.func,
    handleCurrentProduct: PropTypes.func,
    addFavorite:PropTypes.func
}

export default Main