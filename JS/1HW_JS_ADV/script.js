class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const developer1 = new Programmer('Peter', 24, 1000, ['C++','C#']);
const developer2 = new Programmer('Ivan', 30, 2100, ['Java','JavaScript']);
console.log(developer1.name);
console.log(developer1.age);
console.log(developer1.salary);
console.log(developer1.lang);

console.log(developer2.name);
console.log(developer2.age);
console.log(developer2.salary);
console.log(developer2.lang);
