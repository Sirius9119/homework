const list = ["Sumi", "Dnepr", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let parentBody = document.body;

function toList(arr, parent) {
    const ul = document.createElement('ul');
    parent.prepend(ul)
    for (let item of arr) {
        const li = document.createElement('li');
        li.innerText = item;
        ul.appendChild(li);

    }
}

toList(list, parentBody);
