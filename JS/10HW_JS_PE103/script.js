let tabs = document.querySelectorAll('.tabs-title');

for (let tab of tabs) {
    tab.addEventListener('click', function () {
        document.querySelector('.active').classList.remove('active');
        document.querySelector('.content-active').classList.remove('content-active');
        tab.classList.add('active');
        document.querySelector('.content[data-name="' + tab.dataset.name + '"]').classList.add('content-active')

    })
}

