import PropTypes from 'prop-types';
import Modal from "../../components/Modal/index.js";
import ProductSection from "../../components/ProductSection/index.js";
import {useOutletContext} from "react-router-dom"
import {useDispatch} from "react-redux";


function Shop() {
    const {
        addFavorite,
        currentProduct,
        handleCurrentProduct,
        isModal,
        handleModal,
        handleOutside,
        product,
        addBasket
    } = useOutletContext()
    const dispatch = useDispatch()
    return (
        <>
            <ProductSection
                product={product}
                handleModal={handleModal}
                handleCurrentProduct={handleCurrentProduct}
                addFavorite={addFavorite}
            />
            {isModal && <Modal
                title={currentProduct.name}
                text={`Добавить в корзину ${currentProduct.name} за ${currentProduct.price} грн.?`}
                handleOutside={handleOutside}
                closeModal={handleModal}
                yesButton={() => dispatch(addBasket(currentProduct))}
                currentProduct={handleCurrentProduct}

            />}
        </>

    );
}

Shop.propTypes = {};
export default Shop;