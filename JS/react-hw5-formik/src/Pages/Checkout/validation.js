import * as yup from "yup"

export const validationSchema = yup.object({
    firstName: yup
        .string()
        .required('required')
        .min(3, 'name less then 3 symbols')
        .max(20, 'name should be maximum 20 symbols')
        .matches(/[a-zA-Z]/, 'filed name required only letters'),
    lastName: yup
        .string()
        .required('required')
        .min(3, 'lastName less then 3 symbols')
        .max(20, 'lastName should be maximum 20 symbols')
        .matches(/[a-zA-Z]/, 'filed name required only letters'),
    country: yup
        .string()
        .required('required')
        .min(3, 'country less then 3 symbols')
        .max(20, 'country should be maximum 20 symbols')
        .matches(/[a-zA-Z]/, 'filed name required only letters'),
    city: yup
        .string()
        .required('required')
        .min(3, 'city less then 3 symbols')
        .max(20, 'city should be maximum 20 symbols')
        .matches(/[a-zA-Z]/, 'filed name required only letters'),
    streetAddress: yup
        .string()
        .required('required')
        .min(3, 'streetAddress less then 3 symbols')
        .max(20, 'streetAddress should be maximum 20 symbols')
        .matches(/[a-zA-Z]/, 'filed name required only letters'),
    postalCode: yup
        .number()
        .required('required'),
        // .min(4, 'postalCode less then 6 symbols')
        // .max(8, 'postalCode should be maximum 20 symbols'),
    emailAddress: yup
        .string()
        .required('required')
        .email("not email")
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "точка где?"),
    phone: yup
        .number()
        .required('required')
        // .min(9, 'phone less then 10 symbols')
        // .max(15, 'phone should be maximum 15 symbols'),
})