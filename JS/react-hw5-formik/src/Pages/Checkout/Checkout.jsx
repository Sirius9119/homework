import React from 'react';
import PropTypes from 'prop-types';
import './Checkout.scss'
import {useFormik} from "formik";
import InputBox from "../../components/Input/Input.jsx";
import Button from "../../components/Button/Button.jsx";
import {useDispatch, useSelector} from "react-redux";
import {setBuyerInfo, removeBasket} from "../../redux/actions/action.js";
import {validationSchema} from "./validation.js";


function Checkout(props) {
    const buyerInfo = useSelector(({buyerInfo})=>buyerInfo)
    const basket = useSelector(({basket})=>basket)
    const dispatch = useDispatch()
    const formik = useFormik({
        initialValues:{
            ...buyerInfo
    },
        validationSchema,
        onSubmit: (values, {resetForm}) => {
            resetForm();
            dispatch(setBuyerInfo(values))
            console.log(values);
            console.log(basket);
            dispatch(removeBasket())


        }
    }
    )
    return (
        <>
            <div className="section__header">
                <h2 className="section__title">Checkout</h2>
            </div>
            <form onSubmit={formik.handleSubmit}>
                <fieldset className="form__block">
                    <InputBox
                        label={"First Name"}
                        name={'firstName'}
                        placeholder={"First Name"}
                        error={formik.errors.firstName && formik.touched.firstName}
                        errorMessage={formik.errors.firstName}
                        {...formik.getFieldProps('firstName')}
                    />
                    <InputBox
                        label={"Last Name"}
                        name={'lastName'}
                        placeholder={"Last Name"}
                        error={formik.errors.lastName && formik.touched.lastName}
                        errorMessage={formik.errors.lastName}
                        {...formik.getFieldProps('lastName')}
                    />
                    <InputBox
                        label={"Country"}
                        name={'country'}
                        placeholder={"Country"}
                        error={formik.errors.country && formik.touched.country}
                        errorMessage={formik.errors.country}
                        {...formik.getFieldProps('country')}
                    />
                    <InputBox
                        label={"City"}
                        name={'city'}
                        placeholder={"City"}
                        error={formik.errors.city && formik.touched.city}
                        errorMessage={formik.errors.city}
                        {...formik.getFieldProps('city')}
                    />
                    <InputBox
                        label={"Street Address"}
                        name={'streetAddress'}
                        placeholder={"Street Address"}
                        error={formik.errors.streetAddress && formik.touched.streetAddress}
                        errorMessage={formik.errors.streetAddress}
                        {...formik.getFieldProps('streetAddress')}
                    />
                    <InputBox
                        label={"Postal Code"}
                        name={'postalCode'}
                        placeholder={"Postal Code"}
                        error={formik.errors.postalCode && formik.touched.postalCode}
                        errorMessage={formik.errors.postalCode}
                        {...formik.getFieldProps('postalCode')}
                    />
                    <InputBox
                        label={"Email Address"}
                        name={'emailAddress'}
                        placeholder={"Email Address"}
                        error={formik.errors.emailAddress && formik.touched.emailAddress}
                        errorMessage={formik.errors.emailAddress}
                        {...formik.getFieldProps('emailAddress')}
                    />
                    <InputBox
                        label={"Phone Number"}
                        name={'phone'}
                        placeholder={"Phone Number"}
                        error={formik.errors.phone && formik.touched.phone}
                        errorMessage={formik.errors.phone}
                        {...formik.getFieldProps('phone')}
                    />
                </fieldset>
                <div className="col-12">
                    <Button type={"submit"} btnText='Checkout'/>
                </div>
            </form>


        </>
    );
}

Checkout.propTypes = {
    
};
export default Checkout;