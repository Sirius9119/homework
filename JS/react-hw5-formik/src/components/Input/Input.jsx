import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames'
import './Input.scss'

function InputBox({label, type,name, placeholder, error, errorMessage, ...restProps}) {

    return (
        <label className={cn("form__item", {'has__validation': error})}>
            <p className="form__label">{label}</p>
            <input type={type} className="form__control" name={name} placeholder={placeholder} {...restProps}/>
            {error && <p className="error__message">{errorMessage}</p>}
        </label>
    );
}

InputBox.propTypes = {
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    error: PropTypes.object,
    errorMessage: PropTypes.string,
};

InputBox.defaultProps = {
    type: 'text'
}
export default InputBox;