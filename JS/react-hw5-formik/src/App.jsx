import './App.css'

import {Routes, Route} from "react-router-dom";
import Favorite from "./Pages/Favorite";
import Shop from "./Pages/Shop/index.js";
import Basket from "./Pages/Basket/index.js";
import PageWrapper from "./Pages/PageWrapper/index.js";
import Checkout from "./Pages/Checkout/index.js";

function App() {




    return (
        <>
            <Routes>
                <Route path={'/'} element={<PageWrapper/>}>
                    <Route index element={<Shop/>}/>
                    <Route path={'favorite'} element={<Favorite/>}/>
                    <Route path={'basket'} element={<Basket/>}/>
                    <Route path={'checkout'} element={<Checkout/>}/>
                </Route>
            </Routes>

        </>
    )
}

export default App
