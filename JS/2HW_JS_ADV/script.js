const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const parentDiv = document.getElementById('root');

function createListBooks(parent, array) {
    const ul = document.createElement('ul');
    parent.prepend(ul);
    for (let item of array) {
        try {
            if (!item.author) {
                throw new Error(`В объекте ${JSON.stringify(item)} отсуцтвует свойство "author"`);
            }
            if (!item.name) {
                throw new Error(`В объекте ${JSON.stringify(item)} отсуцтвует свойство "name"`);
            }
            if (!item.price) {
                throw new Error(`В объекте ${JSON.stringify(item)} отсуцтвует свойство "price"`);
            }
            const li = document.createElement('li');
            li.innerText = `${item.author} \n ${item.name} \n ${item.price}`;
            ul.appendChild(li);
        } catch (error) {
            console.error(error)
        }
    }
}

createListBooks(parentDiv, books);