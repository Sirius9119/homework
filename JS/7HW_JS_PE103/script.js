function filterBy(arr, type) {
    return arr.filter(e => typeof e !== type);

}

const arr1 = ['string', 48, 'World', "48", true, 'whats up', false, null, 'null'];

console.log(filterBy(arr1, 'string'));
