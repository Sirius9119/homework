//1) Найти все параграфы на странице и установить цвет фона #ff0000
const allTagP = document.getElementsByTagName("p");

for (const element of allTagP) {
    element.style.backgroundColor = '#ff0000';
}
//2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.
console.log(document.getElementById('optionsList'));
console.log(document.getElementById('optionsList').parentElement);
for (const child of document.getElementById('optionsList').childNodes){
    console.log(child.constructor.name);
    console.log(child);
}

//3) Установите в качестве контента элемента testParagraph следующий параграф <p>This is a paragraph</p>
document.getElementById('testParagraph').innerHTML = '<p>This is a paragraph</p>';
//4) Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль.
// Каждому из элементов присвоить новый класс nav-item.
for (const liElement of document.querySelectorAll('.main-header  li')) {
    console.log(liElement);
    liElement.classList.add('nav-item');
}
//5) Найти все элементы с классом section-title. Удалить этот класс у элементов.
for (const element of document.querySelectorAll('.section-title')){
    element.classList.remove('section-title');
}

