const info = document.querySelector('.info')
const btn = document.querySelector('.button');
btn.addEventListener('click', async () => {
    const responseIp = await fetch('https://api.ipify.org/?format=json')
    const {ip} = await responseIp.json();
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=1572889`)
    const address = await response.json()
    info.innerText = `IP адрес: ${ip}\nКонтинент: ${address.continent}\nСтрана: ${address.country}\nРегион: ${address.regionName}\nГород: ${address.city}\nРайон города: ${address.district}\n`;
})