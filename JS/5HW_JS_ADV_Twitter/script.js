class Cards {
    constructor(title, text, user, postId) {
        this.title = title;
        this.text = text;
        this.user = user;
        this.postId = postId;

    }

    createCard() {
        const card = document.createElement("div");
        card.classList.add("post-card");
        card.id = this.postId

        const cardHeader = document.createElement("div");
        cardHeader.classList.add("post-card__header");
        card.appendChild(cardHeader);

        const cardContent = document.createElement("div");
        cardContent.classList.add("post-card__content");
        card.appendChild(cardContent);

        const userName = document.createElement("div");
        userName.classList.add("post-card__user-name");
        userName.textContent = `${this.user.name}`;
        cardHeader.appendChild(userName);

        const userEmail = document.createElement("a");
        userEmail.classList.add("post-card__user-email");
        userEmail.href = "#"
        userEmail.textContent = `${this.user.email}`
        cardHeader.appendChild(userEmail);

        const contentTitle = document.createElement("h3");
        contentTitle.classList.add("post-card__title");
        contentTitle.textContent = this.title;
        cardContent.appendChild(contentTitle);

        const contentText = document.createElement("p");
        contentText.classList.add("post-card__text");
        contentText.textContent = this.text;
        cardContent.appendChild(contentText)

        const deleteBtn = document.createElement("button");
        deleteBtn.classList.add("post-card__delete-btn");
        deleteBtn.innerHTML = "&#10006;"
        deleteBtn.title = "Удалить"
        cardHeader.appendChild(deleteBtn);
        deleteBtn.addEventListener('click', () => {
            this.deleteCard();

        });

        return card;
    }

    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`,{
            method:'DELETE'
        })
            .then((response) =>{
                if(response.ok){
                    document.getElementById(`${this.postId}`).remove();
                }
            } )
            .catch(error => {
                {
                    console.error(error)
                }
            })
    }
}

fetch('https://ajax.test-danit.com/api/json/users',{
    method:'GET'
})
    .then((response) =>response.json())
    .then((users) =>{
        fetch('https://ajax.test-danit.com/api/json/posts',{
            method:'GET'
        })
            .then((response) => response.json())
            .then((posts) =>{
                const cardsContainer = document.querySelector('.container');
                posts.forEach((post) =>{
                    const user = users.find((user => user.id === post.userId))
                    if(user){
                        const card = new Cards(post.title, post.body, user, post.id);
                        cardsContainer.appendChild(card.createCard())
                    }
                })
            })
            .catch((err) =>{
                console.error(err)
            })
    })
    .catch((err) =>{
        console.error(err)
    });