import PropTypes from 'prop-types';
import CardsFavorite from "../../components/CardsFavorite/index.js";
import './Favorite.scss'
import Modal from "../../components/Modal/index.js";
import {useOutletContext} from "react-router-dom";
import {useDispatch} from "react-redux";

function Favorite() {
    const {
        addFavorite,
        currentProduct,
        handleCurrentProduct,
        isModal,
        handleModal,
        handleOutside,
        favorite,
        addBasket
    } = useOutletContext()
    const dispatch = useDispatch()


    return (
        <>
            <div className="favorite__header">
                <h2 className="favorite__title">Favorite</h2>
            </div>
            <div className="favoriteCards__wrapper">
                {favorite.map(({name, price, imageUrl, article, color}) =>
                    <CardsFavorite
                        name={name}
                        price={price}
                        imageUrl={imageUrl}
                        article={article}
                        color={color}
                        key={article}
                        handleModal={handleModal}
                        handleCurrentProduct={handleCurrentProduct}
                        addFavorite={addFavorite}
                    />
                )}
            </div>
            {isModal && <Modal
                title={currentProduct.name}
                text={`Добавить в корзину ${currentProduct.name} за ${currentProduct.price} грн.?`}
                handleOutside={handleOutside}
                closeModal={handleModal}
                yesButton={() => {
                    dispatch(addBasket(currentProduct))
                    dispatch(addFavorite(currentProduct))

                }}
                currentProduct={handleCurrentProduct}

            />}
        </>


    );
}

Favorite.propTypes = {};
export default Favorite;