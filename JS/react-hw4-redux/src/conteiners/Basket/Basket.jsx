import './Basket.scss'
import Modal from "../../components/Modal/index.js";
import CardsBasket from "../../components/CardsBasket/index.js";
import {useOutletContext} from "react-router-dom";
import {useDispatch} from "react-redux";


function Basket(props) {
    const {
        currentProduct,
        handleCurrentProduct,
        isModal,
        handleModal,
        handleOutside,
        deleteBasket,
        basket
    } = useOutletContext()

    const totalCost = basket.reduce((acc, elem) => {
        return acc + elem.price;
    }, 0);

    const dispatch = useDispatch()


    return (

        <>
            <div className="basket__header">
                <h2 className="basket__title">Basket</h2>
            </div>
            <div className="total__wrapper">
                <p className="total__length">{`У Вас в корзине ${basket.length} товаров`}</p>
                <p className="total__price">{`Всего к оплате ${totalCost} грн. `}</p>
                <button className="total__btn">
                    Далее к оплате
                </button>
            </div>
            <div className="basketCards__wrapper">
                {basket.map(({name, price, imageUrl, article, color}, index) =>
                    <CardsBasket
                        name={name}
                        price={price}
                        imageUrl={imageUrl}
                        article={article}
                        color={color}
                        key={`${article}${index}`}
                        handleModal={handleModal}
                        handleCurrentProduct={handleCurrentProduct}
                    />
                )}
            </div>
            {isModal && <Modal
                title={`Удалить из корзины ${currentProduct.name}`}
                text={`Удалить из корзины ${currentProduct.name} за ${currentProduct.price} грн.?`}
                handleOutside={handleOutside}
                closeModal={handleModal}
                yesButton={() =>
                    dispatch(deleteBasket(currentProduct))

                }
                currentProduct={handleCurrentProduct}

            />}
        </>


    );
}

Basket.propTypes = {};
export default Basket;