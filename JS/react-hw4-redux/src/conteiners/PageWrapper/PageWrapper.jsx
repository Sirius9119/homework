import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import Header from "../../components/Header/index.js";
import Footer from "../../components/Footer/index.js";
import {Outlet} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {
    getProduct,
    addFavorite,
    addBasket,
    setCurrentProduct,
    modalVisibility,
    deleteBasket
} from "../../redux/actions/action.js";



function PageWrapper() {
    const dispatch = useDispatch()
    const product = useSelector(({product})=>product)
    const favorite = useSelector(({favorite})=>favorite)
    const basket = useSelector(({basket})=>basket)
    const currentProduct = useSelector(({currentProduct})=>currentProduct)
    const isModal = useSelector(({isModal}) => isModal)

    const handleCurrentProduct = (current) => {
        dispatch(setCurrentProduct({...current}))

    }
    const handleModal = () => {
        dispatch(modalVisibility(isModal))

    }
    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleModal();
        }
    }


    useEffect(() => {
        dispatch(getProduct())
        }, []
    )



    return (
        <>
            <Header
                basketLength={basket.length}
                favoriteLength={favorite.length}
            />
            <main className="main container">

                <Outlet
                    context={{
                        addFavorite,
                        currentProduct,
                        handleCurrentProduct,
                        isModal,
                        handleModal,
                        handleOutside,
                        product,
                        addBasket,
                        favorite,
                        deleteBasket,
                        basket
                    }}
                />
            </main>

            <Footer/>

        </>
    );
}

PageWrapper.propTypes = {};
export default PageWrapper;