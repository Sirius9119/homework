import {
    ADD_FAVORITE,
    GET_PRODUCT,
    ADD_BASKET,
    SET_CURRENT_PRODUCT,
    MODAL_VISIBILITY,
    DELETE_BASKET
} from "../actions/action.js";


const initialState ={
    product:[],
    favorite:JSON.parse(localStorage.getItem('favorite')) || [],
    basket:JSON.parse(localStorage.getItem('basket')) || [],
    currentProduct: {},
    isModal:false,

}

export const rootReducers = (state=initialState, {type,payload})=> {
    switch(type){
        case GET_PRODUCT:
            return {...state, product: payload}
        case ADD_FAVORITE:{
            const {favorite}=state
            if (favorite.some(obj => obj.article === payload.article)) {
                const newFavor = favorite.filter(f => f.article !== payload.article)
                localStorage.setItem('favorite', JSON.stringify([...newFavor]))
            } else {
                localStorage.setItem('favorite', JSON.stringify([...favorite, payload]))
            }

            return {...state, favorite: JSON.parse(localStorage.getItem('favorite'))}

        }
        case ADD_BASKET: {
            const {isModal}=state
            const isBasket = JSON.parse(localStorage.getItem('basket'))
            isBasket ? localStorage.setItem('basket', JSON.stringify([...isBasket, payload]))
                : localStorage.setItem('basket', JSON.stringify([payload]))
            return {...state, basket: JSON.parse(localStorage.getItem('basket')), isModal: !isModal}
            // handleModal();
        }
        case DELETE_BASKET: {
            const {isModal}=state
            const isBasket = JSON.parse(localStorage.getItem('basket'))
            const newBasket = isBasket.filter(f => f.article !== payload.article)
            localStorage.setItem('basket', JSON.stringify([...newBasket]))
            return {...state, basket: JSON.parse(localStorage.getItem('basket')), isModal: !isModal  }
        }
        case SET_CURRENT_PRODUCT:
            return {...state, currentProduct: payload}

        case MODAL_VISIBILITY:
            return {...state, isModal: !payload}





        default:
            return state

    }


}