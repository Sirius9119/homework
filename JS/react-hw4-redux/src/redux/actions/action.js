export const GET_PRODUCT = 'GET_PRODUCT';
export const ADD_FAVORITE = 'ADD_FAVORITE';
export const ADD_BASKET = 'ADD_BASKET';
export const DELETE_BASKET = 'DELETE_BASKET';
export const SET_CURRENT_PRODUCT = 'SET_CURRENT_PRODUCT';
export const MODAL_VISIBILITY = 'MODAL_VISIBILITY';

export const getProduct = () => (dispatch) => {
    return fetch('data.json').then(response => response.json()).then(data => dispatch({
        type: GET_PRODUCT,
        payload: data
    }))
}
export const addFavorite = (payload) => {
    return {
        type: ADD_FAVORITE,
        payload
    }
}
export const addBasket = (payload) => {
    return {
        type: ADD_BASKET,
        payload
    }
}
export const deleteBasket = (payload) => {
    return {
        type: DELETE_BASKET,
        payload
    }

}
export const setCurrentProduct = (payload) => {
    return {
        type: SET_CURRENT_PRODUCT,
        payload
    }
}
export const modalVisibility = (payload) => {
    return {
        type: MODAL_VISIBILITY,
        payload
    }
}
