import { configureStore} from "@reduxjs/toolkit";

import {rootReducers} from "../redusers/reduser.js";


export  default  configureStore( {
        reducer: rootReducers,

    }
)

