import React from 'react'
import './App.css'
import Button from './components/Button'
import Modal from './components/Modal'

class App extends React.Component {
    state= {
        isFirstModal: false,
        isSecondModal: false
    }

    handleFirstModal = () => {
        this.setState(prevState => ({
            isFirstModal:!prevState.isFirstModal,
        }))
    }
    handleSecondModal = () => {
        this.setState(prevState => ({
            isSecondModal:!prevState.isSecondModal,
        }))
    }
    handleOutside = (event) => {
        if(!event.target.closest(".modal")) {
            if(this.state.isFirstModal){
                this.handleFirstModal();
            } else {
                this.handleSecondModal();
            }

        }
    }

    render () {
        const {isFirstModal, isSecondModal} = this.state;

        return (
            <>
                <h1>Neo, выбери кнопку!</h1>
                <Button
                    backgroundColor="button-red"
                    btnText='Красная кнопка'
                    onClick = {this.handleFirstModal}
                />
                <Button
                    backgroundColor="button-blue"
                    btnText='Синяя кнопка'
                    onClick = {this.handleSecondModal}
                />
                {isFirstModal && <Modal
                    title = 'Красная кнопка'
                    text="Выберешь красную кнопку, войдешь в страну чудес!"
                    closeModal = {this.handleFirstModal}
                    handleOutside={this.handleOutside}

                />}
                {isSecondModal && <Modal
                    title = 'Синяя кнопка'
                    text="Выберешь синюю кнопку и сказке конец!"
                    closeModal = {this.handleSecondModal}
                    handleOutside={this.handleOutside}

                />}

            </>
        )
    }
}

export default App
