import React from 'react';
import './Button.scss';

export default  class Button extends React.Component {
    render() {
        const {btnText, backgroundColor, onClick}= this.props

        const defaultClassName = "button";
        const combinedClassName = `${defaultClassName} ${backgroundColor}`;
        return (
            <button
                type="button"
                className={combinedClassName}
                onClick={onClick}
            >
                {btnText}
            </button>

        )
    }
}



