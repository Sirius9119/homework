let images = document.querySelectorAll('.image-to-show');
let current = 0;
let timer = setInterval(slider, 3000)
document.getElementById('stop').onclick = function () {
    clearInterval(timer);
}
document.getElementById('play').onclick = function () {
    timer = setInterval(slider, 3000);
}

function slider() {
    for (let i = 0; i < images.length; i++) {
        images[i].classList.add('hidden');
    }
    images[current].classList.remove('hidden');
    if (current + 1 === images.length) {
        current = 0;
    } else {
        current++;
    }
}


