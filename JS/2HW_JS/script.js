// Цикл нужен для того что бы произвести одно и то же действие н-ное кол-во раз.
//     Например, отсортировать элементы списка, произвести выборку из массива и т.д
"use strict"
let userNumber;
while (!Number.isInteger(userNumber)) {
    userNumber = +prompt('Введите целое число и в консоль будут выведены все числа кратные 5, от 0 до введенного числа');
}
if (userNumber < 5) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 0; i <= userNumber; i += 5) {
        if (i) {
            console.log(i);
        }
    }
}
