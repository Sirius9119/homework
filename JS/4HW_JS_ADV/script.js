const filmsContainer = document.getElementById('root');
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then((data) => {
        data.forEach(film => {
            const filmItem = document.createElement('div');
            const title = document.createElement('h3');
            title.textContent = `${film.episodeId}. ${film.name}`;
            const openingCrawl = document.createElement('p');
            openingCrawl.textContent = film.openingCrawl;
            const charactersList = document.createElement('ul');
            film.characters.forEach(characterUrl => {
                fetch(characterUrl)
                    .then(response => response.json())
                    .then(characterData => {
                        // console.log(characterData)
                        const characterName = document.createElement('li');
                        characterName.textContent = characterData.name;
                        charactersList.appendChild(characterName);
                    });
            });
            filmItem.appendChild(title);
            filmItem.appendChild(openingCrawl);
            filmItem.appendChild(charactersList);
            filmsContainer.appendChild(filmItem);
        });
        // document.body.appendChild(filmsContainer);
        // console.log(data);
    })
    .catch((err) => {
        console.log(err);
    })