import './App.css'
import Header from "./components/Header";
import Footer from "./components/Footer";
import Main from "./components/Main";
import {useState, useEffect} from "react";
import Modal from "./components/Modal";

function App() {
    const [currentProduct, setCurrentProduct] = useState({})
    const [isModal, setIsModal] = useState(false)
    const [product, setProduct] = useState([])
    const handleCurrentProduct = (current) => {
        setCurrentProduct({...current})
    }

    const handleModal = () => {
        setIsModal(!isModal);
    }
    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleModal();
        }
    }


    useEffect(() => {
            fetch('data.json').then(response => response.json()).then(data => setProduct(data))
        }, []
    )
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])
    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem('favorite')) || [])


    const addBasket = (product) => {
        const isBasket = JSON.parse(localStorage.getItem('basket'))
        isBasket ? localStorage.setItem('basket', JSON.stringify([...isBasket, product]))
            : localStorage.setItem('basket', JSON.stringify([product]))
        setBasket(JSON.parse(localStorage.getItem('basket')))
        handleModal();

    }
    const addFavorite = (current) => {
        const isFavorite = JSON.parse(localStorage.getItem('favorite'))
        console.log(JSON.parse(localStorage.getItem('favorite')))

        if (isFavorite.some(obj => obj.article === current.article)) {
            const newFavor = isFavorite.filter(f => f.article !== current.article)
            localStorage.setItem('favorite', JSON.stringify([...newFavor]))
        } else {
            localStorage.setItem('favorite', JSON.stringify([...isFavorite,current]))
        }
        setFavorite(JSON.parse(localStorage.getItem('favorite')))
    }



    return (
        <>
            <Header
                basketLength={basket.length}
                favoriteLength={favorite.length}
            />
            <Main
                product={product}
                handleModal={handleModal}
                handleCurrentProduct={handleCurrentProduct}
                addFavorite={addFavorite}
            />
            <Footer/>
            {isModal && <Modal
                title={currentProduct.name}
                text={`Добавить в корзину ${currentProduct.name} за ${currentProduct.price} грн.?`}
                handleOutside={handleOutside}
                closeModal={handleModal}
                addFavorite={() => addFavorite(currentProduct)}
                yesButton={() => addBasket(currentProduct)}
                currentProduct={handleCurrentProduct}

            />}

        </>
    )
}

export default App
