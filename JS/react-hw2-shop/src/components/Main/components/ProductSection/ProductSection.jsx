import './ProductSection.scss'
import Card from "../Cards";
import PropTypes from 'prop-types'

function ProductSection({product,handleModal, handleCurrentProduct,addFavorite}) {


    return (
        <section className="productSection">
            <div className="productSection__header">
                <h2 className="productSection__title">Categories For Men</h2>
            </div>
            <div className="cards__wrapper">
                {product.map(({name,price, imageUrl, article, color})=>
                <Card
                    name={name}
                    price={price}
                    imageUrl={imageUrl}
                    article={article}
                    color={color}
                    key={article}
                    handleModal={handleModal}
                    handleCurrentProduct={handleCurrentProduct}
                    addFavorite={addFavorite}
                />
                )}

            </div>

        </section>
    )
}

ProductSection.propTypes ={
    product: PropTypes.array,
    handleModal: PropTypes.func,
    handleCurrentProduct: PropTypes.func,
    addFavorite: PropTypes.func
}

export default ProductSection