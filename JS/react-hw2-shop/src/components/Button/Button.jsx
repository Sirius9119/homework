
import './Button.scss';
import PropTypes from 'prop-types'

export default function Button ({btnText, className, onClick}) {

        return (
            <button
                type="button"
                className={className}
                onClick={onClick}
            >
                {btnText}
            </button>

        )

}

Button.propTypes ={
    btnText: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func

}

