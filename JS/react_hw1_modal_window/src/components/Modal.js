import React from 'react';

class Modal extends React.Component {
    render() {
        const { header, closeButton, text, actions } = this.props;

        return (
            <div className="modal">
                <div className="modal-content">
                    {closeButton && (
                        <span className="close" onClick={this.props.onClose}>
              &times;
            </span>
                    )}
                    <h2>{header}</h2>
                    <p>{text}</p>
                    <div className="modal-actions">
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
