import React from 'react';

class Button extends React.Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;

        return (
            <button
                style={{ backgroundColor }}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

export default Button;