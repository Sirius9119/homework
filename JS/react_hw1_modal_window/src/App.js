import React from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstModalOpen: false,
      secondModalOpen: false
    };
  }

  openFirstModal = () => {
    this.setState({ firstModalOpen: true });
  }

  closeFirstModal = () => {
    this.setState({ firstModalOpen: true });
  }

  openSecondModal = () => {
    this.setState({ secondModalOpen: true });
  }

  closeSecondModal = () => {
    this.setState({ secondModalOpen: true });
  }

  render() {
    const { firstModalOpen, secondModalOpen } = this.state;

    return (
        <div>
          <Button
              backgroundColor="blue"
              text="Open first modal"
              onClick={this.openFirstModal}
          />
          <Button
              backgroundColor="green"
              text="Open second modal"
              onClick={this.openSecondModal}
          />

          {firstModalOpen && (
              <Modal
                  header="First Modal"
                  closeButton={true}
                  text="This is the first modal"
                  actions={
                    <div>
                      <Button
                          backgroundColor="blue"
                          text="Action 1"
                      />
                      <Button
                          backgroundColor="blue"
                          text="Action 2"
                      />
                    </div>
                  }
                  onClose={this.closeFirstModal}
              />
          )}

          {secondModalOpen && (
              <Modal
                  header="Second Modal"
                  closeButton={false}
                  text="This is the second modal"
                  actions={
                    <div>
                      <Button
                          backgroundColor="green"
                          text="Button 1"
                      />
                      <Button
                          backgroundColor="green"
                          text="Button 2"
                      />
                    </div>
                  }
                  onClose={this.closeSecondModal}
              />
          )}
        </div>
    );
  }
}

export default App;
