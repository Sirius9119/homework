function createNewUser(
    firstName = prompt('Enter First Name'),
    lastName = prompt('Enter Last Name'),
    birthday = prompt('Enter your date of birth in the format dd.mm.yyyy ')
) {
    this.getLogin = function () {
        return firstName.charAt(0).toLowerCase() + lastName.toLowerCase();
    }
    this.getAge = function () {
        const arrBirth = birthday.split(".");
        const userBirth = new Date(arrBirth[2], arrBirth [1] - 1, arrBirth [0]);
        return parseInt((new Date().getTime() - userBirth.getTime()) / (24 * 3600 * 365.25 * 1000)) || "Вы не правильно ввели дату!";

    }
    this.getPassword = function () {
        return firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.slice((birthday.length - 4));
    }
}

let newUser = new createNewUser;

console.log('Your age', newUser.getAge());
console.log('Your login', newUser.getLogin());
console.log('Your password', newUser.getPassword());




