const eyes = document.querySelectorAll('.InstaIcon-password');
for(let eye of eyes){
    eye.addEventListener('click', (event) => {
        const input = event.target.previousElementSibling
        if (input.type === 'password') {
            input.type = 'text';
            event.target.classList.replace('fa-eye', 'fa-eye-slash');
        } else {
            input.type = 'password';
            event.target.classList.replace('fa-eye-slash', 'fa-eye');
        }

    })
}
const form = document.querySelector('.password-form');
form.addEventListener('submit', (event) => {
    event.preventDefault();
    let pass1 = document.getElementById('password1').value.trim();
    let pass2 = document.getElementById('password2').value.trim();
    if (pass1 === '' || pass2 === '') {
        document.querySelector('.mess').innerText = 'Пароль не может содержать пустую строку!'
        form.reset();
    } else if (pass1 === pass2) {
        alert('You are welcome!');
        form.reset();
    } else {
        document.querySelector('.mess').innerText = 'Нужно ввести одинаковые значения';
        form.reset();
    }
})
form.addEventListener('click', (event)=>{
    document.querySelector('.mess').innerText = '';
})


