import './Header.scss'
import {ReactComponent as Logo} from "./icons/logo.svg";
import {ReactComponent as Favorite} from "./icons/favorite.svg";
import {ReactComponent as Basket} from "./icons/basket.svg";
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";



function Header({basketLength, favoriteLength}) {


    return (
        <header className="header">
            <div className="container">
                <div className="header__wrapper">
                    <div className="header__logo">
                        <a href="#" className="logo">
                            <Logo/>
                        </a>
                    </div>
                    <nav className="header__actions">
                        <Link to="/" className="header__actions-item">Shop</Link>
                        <Link to="/" className="header__actions-item">Men</Link>
                        <Link to="/" className="header__actions-item">Women</Link>
                        <Link to="/" className="header__actions-item">Combos</Link>
                        <Link to="/" className="header__actions-item">Joggers</Link>
                    </nav>
                    <div className="header__favorites-list">

                        <Link to='/favorite' className="icon-favorite">
                            <span className="count-favorite">{favoriteLength}</span>
                            <Favorite/>
                        </Link>
                        <Link to='/basket'  className="icon-basket">
                            <span className="count-basket">{basketLength}</span>
                            <Basket/>
                        </Link>
                    </div>
                </div>
            </div>
        </header>
    )
}

Header.propTypes = {
    basketLength: PropTypes.number,
    favoriteLength: PropTypes.number
}

export default Header