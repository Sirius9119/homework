import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import Header from "../../components/Header/index.js";
import Footer from "../../components/Footer/index.js";
import {Outlet} from "react-router-dom";


function PageWrapper() {
    const [currentProduct, setCurrentProduct] = useState({})
    const [isModal, setIsModal] = useState(false)
    const [product, setProduct] = useState([])
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])
    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem('favorite')) || [])

    const handleCurrentProduct = (current) => {
        setCurrentProduct({...current})
    }
    const handleModal = () => {
        setIsModal(!isModal);
    }
    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleModal();
        }
    }

    const addBasket = (product) => {
        const isBasket = JSON.parse(localStorage.getItem('basket'))
        isBasket ? localStorage.setItem('basket', JSON.stringify([...isBasket, product]))
            : localStorage.setItem('basket', JSON.stringify([product]))
        setBasket(JSON.parse(localStorage.getItem('basket')))
        handleModal();

    }
    const addFavorite = (current) => {
        const isFavorite = favorite
        console.log(JSON.parse(localStorage.getItem('favorite')))

        if (isFavorite.some(obj => obj.article === current.article)) {
            const newFavor = isFavorite.filter(f => f.article !== current.article)
            localStorage.setItem('favorite', JSON.stringify([...newFavor]))
        } else {
            localStorage.setItem('favorite', JSON.stringify([...isFavorite, current]))
        }
        setFavorite(JSON.parse(localStorage.getItem('favorite')))
    }
    const deleteBasket = (product) => {
        const isBasket = JSON.parse(localStorage.getItem('basket'))
        const newBasket = isBasket.filter(f => f.article !== product.article)
        localStorage.setItem('basket', JSON.stringify([...newBasket]))
        setBasket(JSON.parse(localStorage.getItem('basket')))
        handleModal();
    }
    useEffect(() => {
            fetch('data.json').then(response => response.json()).then(data => setProduct(data))
        }, []
    )


    return (
        <>
            <Header
                basketLength={basket.length}
                favoriteLength={favorite.length}
            />
            <main className="main container">

                <Outlet
                    context={{
                        addFavorite,
                        currentProduct,
                        handleCurrentProduct,
                        isModal,
                        handleModal,
                        handleOutside,
                        product,
                        addBasket,
                        favorite,
                        deleteBasket,
                        basket
                    }}
                />
            </main>

            <Footer/>

        </>
    );
}

PageWrapper.propTypes = {};
export default PageWrapper;