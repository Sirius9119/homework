import './App.css'

import {Routes, Route} from "react-router-dom";
import Favorite from "./conteiners/Favorite";
import Shop from "./conteiners/Shop/index.js";
import Basket from "./conteiners/Basket/index.js";
import PageWrapper from "./conteiners/PageWrapper/index.js";

function App() {




    return (
        <>
            <Routes>
                <Route path={'/'} element={<PageWrapper/>}>
                    <Route index element={<Shop/>}/>
                    <Route path={'favorite'} element={<Favorite/>}/>
                    <Route path={'basket'} element={<Basket/>}/>
                </Route>
            </Routes>

        </>
    )
}

export default App
