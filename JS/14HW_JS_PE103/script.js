let style = '';
style = localStorage.getItem('myStyle');
if (style === null) {
    style = 'css/style.css';
}
document.getElementById('styles').setAttribute('href', style);
document.querySelector('.change-style').addEventListener('click', () => {
    if (style === 'css/style.css') {
        document.getElementById('styles').setAttribute('href', 'css/style_new.css');
    } else {
        document.getElementById('styles').setAttribute('href', 'css/style.css');
    }
    style = document.getElementById('styles').getAttribute('href')
    localStorage.setItem('myStyle', style);
})